package com.thoughtworks.springbootemployee.controller;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeReposity employeeReposity = new EmployeeReposity();

    @GetMapping
    public List<Employee> getEmployeeList() {
        return employeeReposity.getEmployees();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeReposity.getEmployees().stream().filter(data -> data.getId() == id).findFirst().orElse(null);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(String gender) {
        return employeeReposity.getEmployees().stream().filter(employee -> gender.equals(employee.getGender())).collect(Collectors.toList());
    }

    @PostMapping
    public Integer createEmployee(@RequestBody Employee employee) {
        employee.setId(generateId());
        employeeReposity.getEmployees().add(employee);
        return employee.getId();
    }

    @PutMapping("/{id}")
    public Employee updateEmployee(@PathVariable int id, @RequestBody Employee employee) {
        Employee employee2 = employeeReposity.getEmployees().stream()
                .filter(employee1 -> employee1.getId() == id)
                .findFirst().orElse(null);
        if (employee2 == null) {
            return null;
        }
        employee2.setAge(employee.getAge());
        employee2.setSalary(employee.getSalary());
        return employee2;
    }

    @DeleteMapping("/{id}")
    public int deleteEmployees(@PathVariable int id) {
        Employee employee1 = employeeReposity.getEmployees().stream().filter(employee -> employee.getId() == id).findFirst().orElse(null);
        if (employee1 == null) {
            return 0;
        }
        employeeReposity.getEmployees().remove(employee1);
        return employee1.getId();
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeeListByPage(int page, int size) {
        return employeeReposity.getEmployees().stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    private Integer generateId() {
        int maxId = employeeReposity.getEmployees().stream().mapToInt(Employee::getId).max().orElse(0);
        return maxId + 1;
    }
}
