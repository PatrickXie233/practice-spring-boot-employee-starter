package com.thoughtworks.springbootemployee.controller;

import java.util.ArrayList;
import java.util.List;

public class EmployeeReposity {
    private List<Employee> employees = new ArrayList<>();

    public EmployeeReposity() {
    }

    public EmployeeReposity(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void addEmploy(Employee employee) {
        this.employees.add(employee);
    }
}
