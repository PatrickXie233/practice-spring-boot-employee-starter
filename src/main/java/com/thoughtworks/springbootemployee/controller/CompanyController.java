package com.thoughtworks.springbootemployee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/companies")
@RestController
public class CompanyController {
    @Autowired
    private EmployeeController employeeController;
    List<Company> companyList = new ArrayList<>();

    @GetMapping
    public List<Company> getCompanyList() {
        return companyList;
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id) {
        return companyList.stream()
                .filter(company -> company.getId() == id).findFirst().orElse(null);
    }

    @PostMapping
    public int createCompany(@RequestBody Company company) {
        company.setId(generateId());
        companyList.add(company);
        return company.getId();
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable int id) {
        return employeeController.getEmployeeList().stream().filter(employee -> employee.getCompanyId() == id).collect(Collectors.toList());
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompanyListByPage(int page, int size) {
        System.out.println(page);
        System.out.println(size);
        return companyList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable int id, @RequestBody Company company) {
        System.out.println(company.getName());
        Company findCompany = companyList.stream()
                .filter(company1 -> company1.getId() == id)
                .findFirst().orElse(null);
        if (findCompany == null) {
            return null;
        }
        findCompany.setName(company.getName());
        return findCompany;
    }
    @DeleteMapping("/{id}")
    public int deleteCompany(@PathVariable int id) {
        Company company = companyList.stream().filter(companyItem -> companyItem.getId() == id).findFirst().orElse(null);
        if (company == null) {
            return 0;
        }
        companyList.remove(company);
        return company.getId();
    }


    private Integer generateId() {
        int maxId = companyList.stream().mapToInt(Company::getId).max().orElse(0);
        return maxId + 1;
    }
}
